(function () {
    'use strict';

    angular.module('app').controller('EditCtrl', Ctrl)

    function Ctrl($http, $routeParams, $location) {
        var vm = this; // View model
        this.contact = {};
        this.submitForm = submitForm;

        init();
        function init() {
            $http.get('api/contacts/' + $routeParams.id).then(function (result){
                vm.contact = result.data;
            });
        }

        function submitForm(){
            if(vm.contact._id) {
                $http.put('api/contacts/' + vm.contact._id, vm.contact).then(function () {
                    $location.path('/search');
                });
            } else {
                $http.post('api/contacts', vm.contact).then(function () {
                    $location.path('/search')
                });
            }
        }
    }

})();