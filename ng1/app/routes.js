(function () {
    'use strict';

    angular.module('app').config(RouteConfig)

    function RouteConfig($routeProvider) {
        $routeProvider.when('/search', {
            templateUrl : 'app/search.html',
            controller : 'SearchCtrl',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : 'app/edit.html',
            controller : 'EditCtrl',
            controllerAs : 'vm'
        }).when("/edit/:id", {
            templateUrl : 'app/edit.html',
            controller : 'EditCtrl',
            controllerAs : 'vm'
        }).otherwise('/search');
    }

})();