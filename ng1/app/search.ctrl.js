(function () {
    'use strict';

    angular.module('app').controller('SearchCtrl', Ctrl);

    function Ctrl($http, modalService) {
        var vm = this;

        this.deleteContact = deleteContact;

        getContacts();

        function getContacts() {
            $http.get('api/contacts').then(function (result) {
                vm.contacts = result.data;
            });
        }

        function deleteContact(id) {
            modalService.confirm().then(function () {
                $http.delete('api/contacts/' + id)
            }).then(getContacts);
        }

    }
})();