'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID;

const Dao = require('./dao.js');

const dao = new Dao();
const app = express();

app.use(bodyParser.json());
app.use(express.static('./'));

app.post('/api/contacts', addContact);
app.get('/api/contacts', getContacts);
app.put('/api/contacts/:id', updateContact);
app.delete('/api/contacts/:id', deleteContact);
app.post('/api/contacts/delete', deleteContacts);

app.use(errorHandler); // after request handlers

var url = 'mongodb://kleesman:123456@ds115799.mlab.com:15799/i399hw';

dao.connect(url)
    .then(() => {
        app.listen(3000, () => console.log('Server is running on port 3000'));
    }).catch(err => {
    console.log("MongoDB connection failed: ");
    console.log(err)
});

function addContact(request, response, next) {
    var contact = request.body;

    var contactData = {
        "name": contact.name,
        "phone": contact.phone
    };

    dao.insert(contactData)
        .then(contacts => response.json(contacts))
        .catch(next);
}

function getContacts(request, response) {
    dao.findAll().then(data => response.json(data));
}

function updateContact(request, response, next) {
    var id = request.params.id;
    var contact = request.body;

    var contactData = {
        "name": contact.name,
        "phone": contact.phone
    };

    dao.update(id, contactData)
        .then(contacts => response.json(contacts))
        .catch(next)
}

function deleteContact(request, response, next) {
    var id = request.params.id;

    dao.remove(id)
        .then(() => response.end())
        .catch(next);
}


function deleteContacts(request, response, next) {
    var ids = request.body;

    dao.removeMany(ids)
        .then(contacts => response.json(contacts))
        .catch(next);
}

function errorHandler(error, request, response, next) {
    console.log(error);
    response.status(500).json({error2: error.toString()});
}
